﻿#include "TbitField.h"

UI TBitField::bitMask(const UI bit) const {
	return ( 1 << (bit & 31) );
}
UI TBitField::indexArr(const UI bit) const {
	return bit >> 5;
}

TBitField::TBitField() {
	CoBit = 1;
	SiArray = (CoBit + 31) >> 5;
	BitArray = new UI [SiArray];
	if (BitArray != NULL) {
		for (UI i = 0; i < SiArray; i++)
			BitArray[i] = 0;
	}
}
TBitField::TBitField(UI _CoLamp) {
	CoBit = _CoLamp;
	SiArray = (CoBit + 31) >> 5;
	BitArray = new UI [SiArray];
	if (BitArray != NULL) {
		for (UI i = 0; i < SiArray; i++) 
			BitArray[i] = 0;
	}
}
TBitField::TBitField(const TBitField& a) {
	CoBit = a.CoBit;
	SiArray = (CoBit + 31) >> 5;
	BitArray = new UI [SiArray];
	if (BitArray != NULL) {
		for (UI i = 0; i < SiArray; i++) 
			BitArray[i] = a.BitArray[i];
	}
}

TBitField::~TBitField() {
	if (SiArray != NULL) {
		delete[] BitArray;
		BitArray = NULL;
		SiArray = 0;
	}
}

UI TBitField::getCoBit(void) const {
	return CoBit;
}

void TBitField::setBit(const UI bit) {
	if( bit < CoBit) {
		BitArray[indexArr(bit)] |= bitMask(bit);
	}
}
void TBitField::delBit(const UI bit) {
	if (bit < CoBit) {
		BitArray[indexArr(bit)] &= ~bitMask(bit);
	}
}
bool TBitField::getBit(const UI bit) {
	if (( BitArray[indexArr(bit)] & bitMask(bit) ) == 0) return false;
	else return true;
}

bool TBitField::operator==(const TBitField& a) const{
	if (CoBit == a.CoBit) {
		for (UI i = 0; i < SiArray; i++) {
			if (BitArray[i] != a.BitArray[i]) return false;
		}
		return true;
	}
	else return false;
}
TBitField& TBitField::operator=(const TBitField& a) {
	if (this != &a) {
		if (CoBit != a.CoBit) {
			delete[] BitArray;
			CoBit = a.CoBit;
			SiArray = (CoBit + 31) >> 5;
			BitArray = new UI [SiArray];
		}
		if (BitArray != NULL) {
			for (UI i = 0; i < SiArray; i++)
				BitArray[i] = a.BitArray[i];
		}
	}
	return *this;
}

TBitField& TBitField::operator!() {
	for (UI i = 0; i < SiArray; i++){
		BitArray[i] = ~BitArray[i];
	}
	return *this;
}
TBitField& TBitField::operator&(const TBitField& a) {
	if (SiArray == a.SiArray) {
		for (UI i = 0; i < SiArray; i++)
			BitArray[i] &= a.BitArray[i];
	}
	return *this;
}
TBitField& TBitField::operator|(const TBitField& a) {
	if (SiArray == a.SiArray) {
		for (UI i = 0; i < SiArray; i++)
			BitArray[i] |= a.BitArray[i];
	}
	return *this;
}


std::istream& operator >> (std::istream& stream, TBitField& obj) {
	using namespace std;

	UI bit;
	int choose;

	do {
		cout << "Input quantity of the bit: ";
		cin >> bit;
	} while (bit <= 0);

	obj = TBitField(bit);

	do {
		system("cls");
		cout << "Select the menu item :" << endl;
		cout << "1. Input all values manyally." << endl;
		cout << "2. Enter all values 1." << endl;
		cout << "3. Enter nothing(all values 0)." << endl << endl;

		cin >> choose;
		
		switch (choose) {
		case 1:
			for (UI i = 0; i < obj.CoBit; i++) {
				do {
					system("cls");
					cout << "Input condition bit[" << i + 1 << "]: ";
					cin >> bit;
				} while (bit > 1 || bit < 0);
				if (bit == 1) obj.setBit(i);
				else obj.delBit(i);
			}
			break;
		case 2:
			system("cls");
			obj = !obj;
			cout << "All values 1!" << endl << endl;
			system("pause");
			choose = 1;
			break;
		case 3:
			system("cls");
			cout << "All values 0!" << endl << endl;
			system("pause");
			choose = 1;
			break;
		default:
			system("cls");
			cout << "You entered an invalid menu item number, available values are from 1 to 3." << endl << endl;
			system("pause");
		}
	} while (choose != 1);

	return stream;
}
std::ostream& operator << (std::ostream& stream,const TBitField& obj) {
	using namespace std;

	TBitField buff(obj);
	system("cls");
	for (UI i = 0; i < buff.CoBit; i++) {
		cout << "Bit[" << i + 1 << "] is ";
		if (buff.getBit(i)) cout << "on." << endl;
		else cout << "off." << endl;
	}
	cout << endl;
	system("pause");
	return stream;
}