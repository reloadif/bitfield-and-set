#ifndef _TSET_
#define _TSET_

#include "TbitField.h"

// UI = typedef unsigned int UI;

class TSet {
	UI maxPower;
	TBitField bitField;

public:
	TSet();
	TSet(UI _mp);
	TSet(const TSet& a);
	TSet(const TBitField& a);

	UI getMaxPower() const;

	void addElem(const UI _el);
	void delElem(const UI _el);
	bool getElem(const UI _el);

	TSet operator+(const UI _el);
	TSet operator-(const UI _el);

	TSet operator+(const TSet& a);
	TSet operator-(const TSet& a);

	TSet& operator!();

	operator TBitField();

	friend std::istream& operator >> (std::istream& stream, TSet& obj);
	friend std::ostream& operator << (std::ostream& stream, const TSet& obj);
};

#endif // !_TSET_

