#include "TSet.h"

TSet::TSet() {
	bitField = TBitField();
	maxPower = bitField.getCoBit();
}
TSet::TSet(UI _mp) {
	bitField = TBitField(_mp);
	maxPower = bitField.getCoBit();
}
TSet::TSet(const TSet& a) {
	maxPower = a.maxPower;
	bitField = a.bitField;
}
TSet::TSet(const TBitField& a) {
	bitField = a;
	maxPower = bitField.getCoBit();
}

UI TSet::getMaxPower() const{
	return maxPower;
}

void TSet::addElem(const UI _el) {
	bitField.setBit(_el);
}
void TSet::delElem(const UI _el) {
	bitField.delBit(_el);
}
bool TSet::getElem(const UI _el) {
	return bitField.getBit(_el);
}

TSet TSet::operator+(const UI _el) {
	TBitField tmp(bitField);
	tmp.setBit(_el);
	return tmp;
}
TSet TSet::operator-(const UI _el) {
	TBitField tmp(bitField);
	tmp.delBit(_el);
	return tmp;
}

TSet TSet::operator+(const TSet& a) {
	TBitField tmp(bitField | a.bitField);
	return tmp;
}
TSet TSet::operator-(const TSet& a) {
	TBitField tmp(bitField & a.bitField);
	return tmp;
}

TSet& TSet::operator!() {
	bitField = !bitField;
	return *this;
}

TSet::operator TBitField() {
	return this->bitField;
}

std::istream& operator >> (std::istream& stream, TSet& obj) {
	using namespace std;

	UI bit, power;

	do {
		system("cls");
		cout << "Enter power of universe: ";
		cin >> power;
	} while (power <= 0);

	obj = TSet(power);

	do {
		system("cls");
		cout << "Enter number of element in set: " << endl;
		cout << "<Press 0 to end input>" << endl << endl;
		cin >> bit;
		obj.addElem(bit-1);

	} while (bit);

	return stream;
}
std::ostream& operator << (std::ostream& stream, const TSet& obj) {
	using namespace std;

	UI maxElem = -1;

	TSet buff(obj);

	system("cls");
	cout << "Set { ";

	for (UI i = 0; i < buff.maxPower; i++)
		if (buff.getElem(i) == true) maxElem = i;
	
	if (maxElem == (UI)-1) { 
		cout << " }" << endl << endl; 
		system("pause"); 
		return stream;
	}

	for (UI i = 0; i < maxElem; i++) {
		if (buff.getElem(i) == true) cout << i + 1 << ", ";
	}
	cout << maxElem+1;
	cout << " }" << endl << endl;
	system("pause");

	return stream;
}